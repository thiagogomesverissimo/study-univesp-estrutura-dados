#include <stdio.h>
#include <malloc.h>

# define alturaMax 40

// struct: comparação coxa com uma classe....
typedef struct {
    int peso;
    int altura;
} Pessoa;

int main () {
    // "instaciando a estrutura..."
    // Ao invés de fazermos: PesoAltura pessoa, vamos criar um ponteiro para uma estrutura
    Pessoa* pessoa = (Pessoa*) malloc(sizeof(Pessoa));
    pessoa->peso = 100;
    pessoa->altura = 56;
    printf("\n Peso: %d e Altura: %d", pessoa->peso, pessoa->altura);

    if (pessoa->altura > alturaMax) {
        printf("\n Rapaz, tu tá acima do peso");
    } else {
        printf("\n Great Job: Você não está acima do peso");
    }
    return 0;
}
