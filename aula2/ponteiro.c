#include<stdio.h>

void main() {

    int x = 10;

    // Referência para uma área da memória do tipo inteiro
    int *y;

    // &x referência a memória onde está guardado x
    y = &x;
    printf("Valor de x: %d\n",x);
    printf("Valor de x acessado por y: %d\n",*y);

    printf("Endereço de x: %d\n",&x);
    printf("Endereço de x armazenado em y: %d\n",y);
}
