#include <stdio.h>

# define alturaMax 40

// struct: comparação coxa com uma classe....
typedef struct {
    int peso;
    int altura;
} Pessoa;

int main () {
    // "instaciando a estrutura..."
    Pessoa pessoa;
    pessoa.peso = 100;
    pessoa.altura = 56;
    printf("\n Peso: %d e Altura: %d", pessoa.peso, pessoa.altura);

    if (pessoa.altura > alturaMax) {
        printf("\n Rapaz, tu tá acima do peso");
    } else {
        printf("\n Great Job: Você não está acima do peso");
    }
    return 0;
}
