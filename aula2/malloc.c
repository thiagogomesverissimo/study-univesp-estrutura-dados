#include<stdio.h>
#include<malloc.h>

int main(){

    // a função malloc retorna um pontero.
    // sizeof recebe um tipo ou estrutura
    // (int*) -> é um cast para inteiro
    int* y = (int*) malloc(sizeof(int));

    *y = 7777;

    printf("y= %d, *y=%d", y,*y);
    return 0;
}
