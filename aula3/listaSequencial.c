# include <stdio.h>
# include <stdlib.h>
# include <malloc.h>

#define MAX 50
#define true 1
#define false 0

typedef int TIPOCHAVE;
typedef int bool;

// Esse seria o tipo de dado para cada registro
typedef struct{
    TIPOCHAVE value;
} VALUE;

// Esse seria o meu tipo de "array" que armazena valores
// do tipo valeu com chave key

typedef struct {
    VALUE A[MAX];
    int n;
} LISTA;

/**
 * Métodos que dever ser implementados:
 * search()
 * delete()
 * restart()
 */
void initialize(LISTA *l);
int size(LISTA *l);
bool insertAtEnd(LISTA *l,VALUE value);
bool insertAt(LISTA *l, VALUE value, int position);
void elements(LISTA *l);
int search(LISTA *l, VALUE value);

void initialize(LISTA *l) {
    l->n = 0;
}

int size(LISTA *l) {
    return l->n;
}

bool insertAtEnd(LISTA *l,VALUE value){
    if(size(l) >= MAX ){
        return false;
    }
    l->A[size(l)] = value;
    l->n++;
    return true;
}

bool insertAt(LISTA *l, VALUE value, int position) {
    if(size(l) >= MAX || position > size(l) || position < 0){
        return false;
    }
    // Desloca todos elementos acima
    for(int i = size(l); i > position ;i--){
        l->A[i] = l->A[i-1];
    }
    l->A[position] = value;
    l->n++;
    return true;
}

void elements(LISTA *l){
    for (int i = 0; i < size(l); i++ ){
        printf("data[%d] = %d\n",i,l->A[i]);
    }
}

int search(LISTA *l, VALUE value) {
    for (int i = 0; i < size(l) ; i++){
        if(value == l->A[i].value) return i;
    }
    return -1;
}

void main (){

    LISTA lista;
    VALUE registro0, registro1, registro2, registro3;

    initialize(&lista);
    printf("Tamanho atual da lista: %d\n",size(&lista));

    registro0.value = 35;
    insertAtEnd(&lista,registro0);

    registro1.value = 101;
    insertAtEnd(&lista,registro1);

    registro2.value = 242;
    insertAtEnd(&lista,registro2);

    printf("Tamanho atual da lista: %d\n",size(&lista));
    elements(&lista);

    registro3.value = 500;
    insertAt(&lista,registro3,1);
    printf("Tamanho atual da lista: %d\n",size(&lista));
    elements(&lista);

    printf("Posição de 500 na lista: %d\n",search(&lista,registro3));
}
